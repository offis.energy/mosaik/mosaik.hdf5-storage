# Mosaik.HDF5-Storage

Store any simulator metric into a HDF5 database using a common scheme.

## Status

[![pipeline status](https://gitlab.com/offis.energy/mosaik/mosaik.hdf5_semver/badges/master/pipeline.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.hdf5_semver/pipelines)
[![coverage report](https://gitlab.com/offis.energy/mosaik/mosaik.hdf5_semver/badges/master/coverage.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.hdf5_semver/-/jobs)
[![libraries status](https://img.shields.io/librariesio/release/pypi/mosaik.HDF5-semver)](https://libraries.io/pypi/mosaik.HDF5-semver)
[![Python Versions](https://img.shields.io/pypi/pyversions/mosaik.HDF5-semver)](https://pypi.org/project/mosaik.HDF5-semver)
[![license badge](https://img.shields.io/pypi/l/mosaik.HDF5-semver)](#)
