import os
from concurrent.futures.process import ProcessPoolExecutor
from concurrent.futures.thread import ThreadPoolExecutor
from copy import copy

import numpy

from mosaik_hdf5_storage.hdf5_storage.library.get_hdf5_filepath_module import \
    get_hdf5_filepath
from mosaik_hdf5_storage.hdf5_storage.reader import Hdf5Reader
from mosaik_hdf5_storage.hdf5_storage.writer import Hdf5Writer
from test.conftest import mock_write


def test_hdf5_delete_write_other_metric_writer_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name='another-' + metric_name,
        data=data,
    )

    # Test
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, read_back)


def test_hdf5_delete_write_other_data_writer_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )
    data_modified = copy(data)
    data_modified.update({'other_key': 'other_value'})
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data_modified,
    )

    # Test
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data_modified, read_back)


def test_hdf5_delete_writer_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )

    # Test
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, read_back)


def test_hdf5_delete_writer_writer_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )

    # Test
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )

    # Assert
    data_read = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, data_read)


def test_hdf5_delete_writer_stress_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )

    # Test
    for _ in range(100):
        Hdf5Writer.write(
            base_path=base_path,
            simulator=simulator,
            datetime_object=datetime_object,
            metric_name=metric_name,
            data=data,
        )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, read_back)


def test_hdf5_delete_writer_stress_multi_threaded_reader(
    base_path, simulator, datetime_object, metric_name, data
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )

    # Test
    with ThreadPoolExecutor() as executor:
        for _ in range(100):
            executor.submit(
                mock_write,
                base_path,
                simulator,
                datetime_object,
                metric_name,
                data,
            )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, read_back)


def test_delete_hdf5_writer_stress_multi_processed_reader(
    base_path, simulator, datetime_object, metric_name, data,
):
    # Mock
    os.remove(
        get_hdf5_filepath(
            base_path=base_path,
            simulator=simulator,
        )
    )

    # Test
    with ProcessPoolExecutor() as executor:
        for _ in range(100):
            executor.submit(
                mock_write,
                base_path,
                simulator,
                datetime_object,
                metric_name,
                data,
            )

    # Assert
    read_back = Hdf5Reader.read(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
    )

    numpy.testing.assert_equal(data, read_back)
