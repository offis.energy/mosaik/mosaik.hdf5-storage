from pytest import fixture

from mosaik_hdf5_storage.hdf5_storage.mock.mock_simulator import MockSimulator


@fixture
def mock_simulator() -> MockSimulator:
    mock_simulator: MockSimulator = MockSimulator()

    return mock_simulator
