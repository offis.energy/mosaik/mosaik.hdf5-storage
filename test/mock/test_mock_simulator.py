def test_mock_simulator_instantiate(mock_simulator):
    pass


def test_mock_simulator_create(mock_simulator):
    # Mock
    num = 1
    model = 'MockModel'

    # Test
    mock_simulator.create(
        num=num,
        model=model,
    )


def test_mock_simulator_step(mock_simulator):
    # Mock
    inputs = {}
    time = 0

    # Test
    mock_simulator.step(
        time=time,
        inputs=inputs,
    )


def test_mock_simulator_get_data(mock_simulator):
    # Mock
    outputs = {}

    # Test
    mock_simulator.get_data(
        outputs=outputs,
    )
