from pathlib import Path

from mosaik_hdf5_storage.hdf5_storage.library.get_hdf5_filepath_module import \
    get_hdf5_filepath


def test_get_hdf5_filepath(base_path, simulator):
    # Test
    file_path = get_hdf5_filepath(
        base_path=base_path,
        simulator=simulator,
    )

    # Assert
    assert file_path
    assert isinstance(file_path, Path)
    assert str(file_path.relative_to(Path(__file__).parent.parent)) == \
        'data/MockSimulator.h5'
