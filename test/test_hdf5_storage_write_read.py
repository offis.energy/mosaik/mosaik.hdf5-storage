import hdf5storage
import numpy

from mosaik_hdf5_storage.hdf5_storage.library.get_path_module import get_path
from mosaik_hdf5_storage.hdf5_storage.library.get_options_module import \
    get_options


def test_hdf5storage_write_read(
    data, hdf5_filepath, datetime_object, metric_name,
):
    # Mock
    path: str = get_path(
        datetime_object=datetime_object,
        metric_name=metric_name,
    )
    filename = str(hdf5_filepath)
    options = get_options()

    # Test
    hdf5storage.write(
        data=data,
        path=path,
        filename=filename,
        options=options,
    )
    read_back = hdf5storage.read(
        path=path,
        filename=filename,
    )

    # Assert
    numpy.testing.assert_equal(data, read_back)


def test_hdf5storage_write_write_read(
    data, hdf5_filepath, datetime_object, metric_name,
):
    # Mock
    path: str = get_path(
        datetime_object=datetime_object,
        metric_name=metric_name,
    )
    filename = str(hdf5_filepath)
    options = get_options()

    # Test
    hdf5storage.write(
        data=data,
        path=path,
        filename=filename,
        options=options,
    )
    hdf5storage.write(
        data=data,
        path=path,
        filename=filename,
        options=options,
    )
    read_back = hdf5storage.read(
        path=path,
        filename=filename,
    )

    # Assert
    numpy.testing.assert_equal(data, read_back)
