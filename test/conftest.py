from datetime import datetime
from pathlib import Path

import numpy
from mosaik_api import Simulator
from pytest import fixture

from mosaik_hdf5_storage.hdf5_storage.library.get_hdf5_filepath_module import \
    get_hdf5_filepath
from mosaik_hdf5_storage.hdf5_storage.mock.mock_simulator import MockSimulator
from mosaik_hdf5_storage.hdf5_storage.util.paths import DATA_PATH
from mosaik_hdf5_storage.hdf5_storage.writer import Hdf5Writer


@fixture(scope='session')
def simulator() -> Simulator:
    simulator: Simulator = MockSimulator()

    return simulator


@fixture(scope='session')
def base_path() -> Path:
    base_path: Path = DATA_PATH

    return base_path


@fixture
def hdf5_filepath(simulator, base_path) -> Path:
    hdf5_filepath: Path = get_hdf5_filepath(
        simulator=simulator,
        base_path=base_path,
    )

    return hdf5_filepath


@fixture(scope='session')
def datetime_object() -> datetime:
    datetime_object: datetime = datetime.utcnow()
    return datetime_object


@fixture(scope='session')
def metric_name() -> str:
    metric_name: str = 'mock_metric'

    return metric_name


@fixture(scope='session')
def data() -> dict:
    data: dict = {
        'a': True,
        'b': None,
        'c': 2,
        'd': -3.2,
        'e': (1 - 2.3j),
        'f': 'hello',
        'g': b'goodbye',
        'h': ['list', 'of', 'stuff', [30, 2.3]],
        'i': numpy.zeros(shape=(2,), dtype=[('bi', 'uint8')]),
        'j': {
            'aa': numpy.bool_(False),
            'bb': numpy.uint8(4),
            'cc': numpy.uint32([70, 8]),
            'dd': numpy.int32([]),
            'ee': numpy.float32([[3.3], [5.3e3]]),
            'ff': numpy.complex128([[3.4, 3], [9 + 2j, 0]]),
            'gg': numpy.array(['one', 'two', 'three'], dtype='str'),
            'hh': numpy.bytes_(b'how many?'),
            # 'ii': numpy.object_(['text', numpy.int8([1, -3, 0])])
        }
    }

    return data


def mock_write(
    base_path,
    simulator,
    datetime_object,
    metric_name,
    data
):
    Hdf5Writer.write(
        base_path=base_path,
        simulator=simulator,
        datetime_object=datetime_object,
        metric_name=metric_name,
        data=data,
    )
