import datetime

from setuptools import setup, find_namespace_packages

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

LONG_DESCRIPTION = \
    open('README.md').read()

NAMESPACE = 'mosaik_hdf5_storage'

setup(

    name='mosaik.HDF5-Storage',
    version='0.1.0' + 'rc' + TIMESTAMP,
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    description=(
        'Store any simulator metric into a HDF5 database using a common scheme.'
    ),
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',
    install_requires=[
        'h5py>=3.1.0',
    ],
    py_modules=[],
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    namespace_packages=[
        NAMESPACE,
    ],
    packages=find_namespace_packages(include=[NAMESPACE + '.*']),
    package_dir={'': '.'},
)
